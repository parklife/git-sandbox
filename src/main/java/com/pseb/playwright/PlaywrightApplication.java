package com.pseb.playwright;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.BrowserType;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class PlaywrightApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(PlaywrightApplication.class, args);

	    Playwright playwright = Playwright.create();
		BrowserType.LaunchOptions launchOptions = new BrowserType.LaunchOptions().setHeadless(true);
		Browser browser = playwright.chromium().launch(launchOptions);
		Page page = browser.newPage();
		page.navigate("http://playwright.dev");
		System.out.println("###### THIS IS A SIGN #####" + page.title());
		browser.close();
		playwright.close();

		SpringApplication.exit(ctx, () -> 0);
	}
}
